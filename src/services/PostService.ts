import type IPost from '@/interfaces/IPost' //Interfaz
import { ref } from 'vue' // función de reactividad
import type { Ref } from 'vue' // La interfaz de reactividad (ref)

//AQUI ES PARA OBTENER LA INFORMACIÓN DEL ENV
const url = import.meta.env.VITE_API_URL || 'https://jsonplaceholder.typicode.com'

class Botella {
  private contenido: string
  private marca: string
  private tipo: string

  constructor(contenido:string,marca:string,tipo:string) {
    this.contenido = contenido
    this.marca = marca
    this.tipo = tipo
  }
}

const cocacola = new Botella('Agua carbonatada color oscura','Coca Cola','Cristal')
const pepsi = new Botella('Agua carbonatada color oscura','Pepsi','Plastico')

//Declarando y exportando la clase
export default class PostService {
  //Datos reactivos
  private posts: Ref<IPost[]>
  private post: Ref<IPost>

  //AQUI SE INICIALIZAN LOS DATOS
  constructor() {
    this.posts = ref([])
    this.post = ref({}) as Ref<IPost>
  }
  //Son Los getters
  getPosts(): Ref<IPost[]> {
    return this.posts
  }
  getPost(): Ref<IPost> {
    return this.post
  }

  async fetchAll(): Promise<void> {
    try {
      const json = await fetch(url + '/posts')
      const response = await json.json()
      this.posts.value = await response
    } catch (error) {
      //Agrega aqui lo que necesites
      console.log(error)
    }
  }

  async fetchPost(id: string): Promise<void> {
    try {
      const json = await fetch(url + '/posts/' + id)
      const response = await json.json()
      this.post.value = await response
    } catch (error) {
      console.log(error)
    }
  }
}
